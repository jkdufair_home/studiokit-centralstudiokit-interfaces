﻿using System;
using System.Collections.Generic;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IPersonRepository
	{
		IPerson Add(IPerson person);

		IPerson PersonWithCredentials(string identifier, string password);

		IPerson GetByUsername(string username);

		IPerson GetByEmailAddress(string emailAddress);

		IPerson GetById(int id);

		IEnumerable<IPerson> GetWithUsernames(IEnumerable<string> usernames);

		bool ExistsByUsername(string username);

		bool Delete(IPerson person);

		void AddOrUpdateSetting(int personid, string type, string value);

		IPerson GetByOAuthProviderUserId(string userId);

		List<IUserSetting> GetSettingsByValue(string value);

		List<IUserSetting> GetSettingsByUserId(int userId);

		IPerson GetOrCreateFromProvider(string providerUserId, string providerName,
																		Func<string, IPerson> providerUserResolver);

		void AddUserToRole(IPerson user, string role);
	}
}