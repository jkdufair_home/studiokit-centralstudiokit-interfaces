﻿namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IUserSetting
	{
		int Id { get; set; }

		int UserId { get; set; }

		string SettingsType { get; set; }

		string Value { get; set; }
	}
}