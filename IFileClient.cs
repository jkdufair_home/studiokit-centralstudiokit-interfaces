﻿using System;
using System.Collections.Generic;
using System.IO;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IFileClient
	{
		void Save(string path, Stream stream);

		void Copy(string sourcePath, string destinationPath);

		Stream Open(string path);

		void Delete(string path);

		bool Exists(string path);

		string GetUrl(string path);

		IEnumerable<string> EnumerateFiles(string path);

		IEnumerable<string> EnumerateFiles(string path, string searchPattern);

		IEnumerable<string> EnumerateFiles(string path, string searchPattern, SearchOption searchOption);

		DateTime GetLastWriteTime(string path);

		long GetLength(string path);
	}
}