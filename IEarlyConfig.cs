﻿namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IEarlyConfig
	{
		string ReadConfig(string key);
	}
}