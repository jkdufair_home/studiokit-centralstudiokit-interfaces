﻿using System.Collections.Generic;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IEmailFactory
	{
		void SendEmail(string fromIn, string subjectIn, string plainbodyIn, string htmlbodyIn, IEnumerable<string> addressesIn);

		void SendEmail(string fromIn, string subjectIn, string plainbodyIn, string htmlbodyIn, string emailAddress);
	}
}