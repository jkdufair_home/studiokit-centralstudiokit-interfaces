﻿using System;
using System.Collections.Generic;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface ICacheClient
	{
		object Get(string key);

		T Get<T>(string key);

		IEnumerable<KeyValuePair<string, object>> GetMany(IEnumerable<string> keys);

		bool Put(string key, object value);

		bool Put(string key, object value, TimeSpan validFor);

		long Increment(string key, long delta, long initialValue);

		long Decrement(string key, long delta, long initialValue);

		bool Remove(string key);

		void PurgeByPrefix(string prefix);

		void PurgeByPrefixList(IEnumerable<string> prefixEnumerable);

		void PurgeByContains(string contains);

		void PurgeByContainsList(IEnumerable<string> contiansEnumerable);

		void Flush();
	}
}