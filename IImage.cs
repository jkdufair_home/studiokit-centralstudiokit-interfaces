﻿using System;

namespace StudioKit.CentralStudioKit.Interfaces
{
	public interface IImage
	{
		int Id { get; set; }
		int OwnerId { get; set; }
		int Size { get; set; }
		string Path { get; set; }
		DateTime CreatedDate { get; set; }
	}
}